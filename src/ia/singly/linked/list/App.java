package ia.singly.linked.list;

public class App {

	public static void main(String[] args) {
		SinglyLinkedList<Integer> sl = new SinglyLinkedList<>();
		sl.insertFirst(1);
		sl.insertFirst(2);
		sl.insertFirst(3);
		sl.insertFirst(5);
		sl.insertFirst(55);
		sl.insertLast(88888);
		sl.displayList();
		System.out.println("last is : " + sl.getLast().getData());
	}
}
