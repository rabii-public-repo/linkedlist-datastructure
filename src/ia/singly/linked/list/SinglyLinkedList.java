package ia.singly.linked.list;

public class SinglyLinkedList<T> {
	private Node<T> first;
	private Node<T> last;

	public SinglyLinkedList() {
		first = null;
		last = null;
	}

	void insertFirst(T data) {
		Node<T> newNode = new Node<T>(data);
		if (isEmpty()) {
			first = newNode;
			last = newNode;
			return;
		}
		newNode.setNext(first);
		first = newNode;
	}

	void insertLast(T data) {
		Node<T> newNode = new Node<T>(data);
		if (isEmpty()) {
			first = last = newNode;
			return;
		}
		last.setNext(newNode);
		last = newNode;
	}

	void delete(T data) {
		if (isEmpty()) {
			System.out.println("empty list");
			return;
		}
		Node<T> current = first;
		Node<T> prev = null;
		while (current.getData() != data) {
			prev = current;
			current = current.getNext();
			if (current == null) {
				System.out.println(data + " is not founded in the list");
				return;
			}
		}
		if (current.getData() == data) {
			prev.setNext(current.getNext());
			current = null;
		}
	}

	void displayList() {
		Node<T> current = first;
		if (current == null) {
			System.out.println("Empty list");
			return;
		}
		System.out.println("first - > last");
		while (current != null) {
			System.out.println(current.getData());
			current = current.getNext();
		}
	}

	Node<T> getLast() {
		return this.last;
	}

	boolean isEmpty() {
		return first == null;
	}
}
